const SITE_TEMPLATE_PATH = "/local/templates/main/";


$('#get_discount').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: SITE_TEMPLATE_PATH+"/include/pages/promo/ajax/result_get_discount.php",
        success: function (a) {
            $('#result_get_discount').html(a);
        }
    });
});

$('#check_discount').on('submit', function (e) {
    e.preventDefault();
	let promo = $('input[name=promo]').val();
    $.ajax({
        type: "POST",
        url: SITE_TEMPLATE_PATH+"/include/pages/promo/ajax/check_discount.php",
	    data: ({
	      "promo": promo
	    }),
        success: function (a) {
            $('#result_check_discount').html(a);
        }
    });
});