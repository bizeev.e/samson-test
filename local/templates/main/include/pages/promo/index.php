<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/include/pages/promo/js/promo.js');


global $USER;
if (!$USER->IsAuthorized()) LocalRedirect("/");
?>
<form id="get_discount">
  <input type="submit" value="Получить скидку">
</form>

<div id="result_get_discount"></div>


<form id="check_discount">
    <input type="text" name="promo">
    <input type="submit" value="Проверить скидку">
</form>

<div id="result_check_discount"></div>
