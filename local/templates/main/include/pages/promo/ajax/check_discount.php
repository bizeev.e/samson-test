<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;
use Bitrix\Sale\Internals;
use Bitrix\Main\Type\DateTime;
 
\Bitrix\Main\Loader::includeModule('sale');

$request = Application::getInstance()->getContext()->getRequest();

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && $request->isPost())
{

  $promo = $request->getPost('promo');

  $result = '';



  $date = new DateTime();

  $discountIterator = Internals\DiscountCouponTable::getList(array(
       'select' => array('DISCOUNT_ID'),
       'filter' => array(
              "ACTIVE" => "Y",
              "USER_ID" => $USER->GetID(),
              "COUPON" => $promo,
              ">=ACTIVE_TO" => $date,
            )
   ));
   if ($discount = $discountIterator->fetch())
   {

    $discountSale = \Bitrix\Sale\Internals\DiscountTable::getList(array(
         'select' => array('ID',"ACTIONS_LIST"),
         'filter' => array('ID' => $discount["DISCOUNT_ID"])
     ));
     if ($discountValue = $discountSale->fetch())
     {
      $result =  "Скидка ".$discountValue['ACTIONS_LIST']['CHILDREN'][0]['DATA']['Value']."%";
     }
     else
     {
         $result =  "Скидка недоступна";
     }
   }
   else
   {
       $result = "Скидка недоступна";
   }

  echo $result;

}