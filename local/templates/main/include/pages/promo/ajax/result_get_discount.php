<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Sale\Internals;
use Bitrix\Main\Type\DateTime;
 
\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('catalog');

$result = '';

$date = new DateTime();

$date2 = new DateTime();
$date2->add("-1 hour");

$discountIterator = Internals\DiscountCouponTable::getList(array(
     'select' => array('COUPON','DISCOUNT_ID'),
     'order' => array('ACTIVE_FROM' => 'DESC'),
     'filter' => array(
            "ACTIVE" => "Y",
            "USER_ID" => $USER->GetID(),
            ">ACTIVE_FROM" => $date2,
            ">=ACTIVE_TO" => $date,
          )
 ));
 if ($discount = $discountIterator->fetch())
 {


  $discountSale = \Bitrix\Sale\Internals\DiscountTable::getList(array(
       'select' => array('ID',"ACTIONS_LIST"),
       'filter' => array('ID' => $discount["DISCOUNT_ID"])
   ));
   if ($discountValue = $discountSale->fetch())
   {
      $result .= '<p>Ваша скидка '.$discountValue['ACTIONS_LIST']['CHILDREN'][0]['DATA']['Value'].'%</p>';
   }

   $result .= '<p>Купон на скидку '.$discount["COUPON"].'</p>';

 }else{
    $discountValue = rand(1, 50);
    $result .= '<p>Ваша скидка '.$discountValue.'%</p>';
     

    $dateStart = new DateTime();

    $dateEnd = new DateTime();
    $dateEnd->add("+3 hour");

     
    // Формируем массив для добавления правила
    $arFields = array(
            "LID" => "s1",
            "NAME" => "Скидка ".$discountValue."%",
            "CURRENCY" => "RUB",
            "ACTIVE" => "Y",
            "USER_GROUPS" => array(2),
            "ACTIVE_FROM" => $dateStart,
            "ACTIVE_TO" =>  $dateEnd,
               'ACTIONS' => [
                  "CLASS_ID" => "CondGroup",
                  "DATA" => [
                     "All" => "AND"
                  ],
                  "CHILDREN" => [
                     [
                        "CLASS_ID" => "ActSaleBsktGrp",
                        "DATA" => [
                           "Type" => "Discount",
                           "Value" => $discountValue,
                           "Unit" => "Perc",
                           "Max" => 0,
                           "All" => "OR",
                           "True" => "True",
                        ],
                        "CHILDREN" => [
                           [
                              "CLASS_ID" => "ActSaleSubGrp",
                              "DATA" => [
                                 "All" => "AND",
                                 "True" => "True",
                              ]
                           ]
                        ]
                     ]
                  ]
               ],
               "CONDITIONS" =>  [
                  'CLASS_ID' => 'CondGroup',
                  'DATA' => [
                     'All' => 'AND',
                     'True' => 'True',
                  ],
                  'CHILDREN' => [
                     [
                        "CLASS_ID" => "CondBsktProductGroup",
                        "DATA" => [
                           "Found" => "Found",
                           "All" => "OR",
                        ]
                     ],
                  ],  
               ]
        );
     
    // добавление нового правило скидок
    $discountID = \CSaleDiscount::Add($arFields);


    if ($discountID > 0) {
     
        $codeCoupon = CatalogGenerateCoupon();
     
        $couponFields = array(
                        "DISCOUNT_ID" => $discountID,
                        "COUPON" => $codeCoupon,
                        "ACTIVE" => "Y",
                        "ACTIVE_FROM_FILTER_PERIOD" => "interval",
                        "ACTIVE_FROM" => $dateStart,
                        "ACTIVE_TO" => $dateEnd,
                        "USER_ID" => $USER->GetID(),
                        "TYPE" => 2,
                        "MAX_USE" => 1
                    );
     
        // добавляем новый купон
        $addCouponRes = Internals\DiscountCouponTable::add($couponFields);
        if (!$addCouponRes->isSuccess()){
             
            $err = $addCouponRes->getErrorMessages();
            print_r($err);
             
        } else {
            $result .= '<p>Купон на скидку '.$codeCoupon.'</p>';
        }
    } else {
        $ex = $APPLICATION->GetException();  
        echo 'Ошибка при создании нового правила скидок: '.$ex->GetString();
    }   
 }

echo $result;

